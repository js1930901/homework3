/* 
 Теоретичні питання
 1. Що таке логічний оператор?
Логічні оператори дозволяють
порівнювати змінні та виконувати певні дії на основі результату цього
порівняння. Наприклад, якщо результат порівняння є true, ви можете виконати блок
коду; якщо він є false, ви можете виконати інший блок коду.
JavaScript надає три логічні оператори:


 2. Які логічні оператори є в JavaScript і які їх символи?

 JavaScript надає три логічні оператори:
  ! (Логічне НЕ)
  || (Логічне АБО)
  && (Логічне І)

 
 Практичні завдання.
 1. Попросіть користувача ввести свій вік за допомогою prompt. 
 Якщо вік менше 12 років, виведіть у вікні alert повідомлення про те, 
 що він є дитиною, якщо вік менше 18 років, виведіть повідомлення про те, 
 що він є підлітком, інакше виведіть повідомлення про те, що він є дорослим.

 Завдання з підвищенною складністю.
 Перевірте чи ввів користувач саме число в поле prompt. Якщо ні, то виведіть повідомлення, що введено не число.

 2. Напишіть програму, яка запитує у користувача місяць року (українською мовою маленкими літерами) та виводить повідомлення, 
 скільки днів у цьому місяці. Результат виводиться в консоль.
 Скільки днів в кожному місяці можна знайти тут в розділі Юліанський і Григоріанський календарі - https://uk.wikipedia.org/wiki/%D0%9C%D1%96%D1%81%D1%8F%D1%86%D1%8C_(%D1%87%D0%B0%D1%81)
 (Використайте switch case)
 */

let age = prompt('Вкажіть свій вік')

if (age == null || age == '' || isNaN(age)) {
  alert('Ви ввели не число')
} else {
  if (age < 12) {
    alert('Ви ще дитина')
  } else if (age < 18) {
    alert('Ви є підлітком')
  } else {
    alert('Ви є дорослим')
  }
}

let month = prompt(
  'Вкажіть місьця року (українською мовою і маленькими літерами'
)
// month = month.toLowerCase()
switch (month) {
  case 'січень':
    console.log(`В цьому місяці 31 день`)
    break
  case 'лютий':
    console.log(`В цьому місяці 28 день`)
    break
  case 'березень':
    console.log(`В цьому місяці 31 день`)
    break
  case 'квітень':
    console.log(`В цьому місяці 30 день`)
    break
  case 'травень':
    console.log(`В цьому місяці 31 день`)
    break
  case 'червень':
    console.log(`В цьому місяці 30 день`)
    break
  case 'липень':
    console.log(`В цьому місяці 31 день`)
    break
  case 'серпень':
    console.log(`В цьому місяці 31 день`)
    break
  case 'вересень':
    console.log(`В цьому місяці 30 день`)
    break
  case 'жовтень':
    console.log(`В цьому місяці 31 день`)
    break
  case 'листопад':
    console.log(`В цьому місяці 30 день`)
    break
  case 'грудень':
    console.log(`В цьому місяці 31 день`)
    break
  default:
    console.log(`Такого місяця не існує`)
}
